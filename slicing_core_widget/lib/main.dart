import 'package:flutter/material.dart';
import 'package:slicing_core_widget/shared/theme.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => const SplashScreenPage(),
        '/login': (context) => const LoginPage(),
        '/home': (context) => const HomePage(),
        '/profile': (context) => const ProfilePage(),
        '/detail':(context)=>const DetailActivityPage(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
