part of './theme.dart';

class BottomNavbar extends StatelessWidget {
  final bool homeIsSelected;
  final bool saveIsSelected;
  final bool chatIsSelected;
  final bool profileIsSelected;
  const BottomNavbar({ Key? key, this.homeIsSelected = false,this.saveIsSelected = false,this.chatIsSelected = false,this.profileIsSelected = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 68,
        color: whiteColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            InkWell(
              onTap: ()=>Navigator.pushNamed(context, '/home'),
              child: Column(
                children: [
                  const SizedBox(height: 13),
                  SizedBox(
                    width: 24,
                    height: 24,
                    child: Icon(
                      Icons.home_filled,
                      color: homeIsSelected ? primaryColor : grey1Color,
                    ),
                  ),
                  Text(
                    'Home',
                    style: homeIsSelected ? primaryTextStyle.copyWith(fontWeight: bold): grey1TextStyle.copyWith(fontWeight: bold),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: (){},
              child: Column(
                children: [
                  const SizedBox(height: 13),
                  SizedBox(
                    width: 24,
                    height: 24,
                    child: Icon(
                      Icons.favorite,
                      color: saveIsSelected ? primaryColor : grey1Color,
                    ),
                  ),
                  Text(
                    'Save',
                     style: saveIsSelected ? primaryTextStyle.copyWith(fontWeight: bold): grey1TextStyle.copyWith(fontWeight: bold),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: (){},
              child: Column(
                children: [
                  const SizedBox(height: 13),
                  SizedBox(
                    width: 24,
                    height: 24,
                    child: Icon(
                      Icons.chat,
                     color: chatIsSelected ? primaryColor : grey1Color,
                    ),
                  ),
                  Text(
                    'Chat',
                 style: chatIsSelected ? primaryTextStyle.copyWith(fontWeight: bold): grey1TextStyle.copyWith(fontWeight: bold),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: ()=>Navigator.pushNamed(context, '/profile'),
              child: Column(
                children: [
                  const SizedBox(height: 13),
                  SizedBox(
                    width: 24,
                    height: 24,
                    child: Icon(
                      Icons.person_rounded,
                   color: profileIsSelected ? primaryColor : grey1Color,
                    ),
                  ),
                  Text(
                    'Profile',
                      style: profileIsSelected ? primaryTextStyle.copyWith(fontWeight: bold): grey1TextStyle.copyWith(fontWeight: bold),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
      
  }
}