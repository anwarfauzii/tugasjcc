part of '../shared/theme.dart';

class IconBar extends StatelessWidget {
  final String imageUrl;
  final double left;
  final double right;
  final double top;
  final double bottom;
  final double square;
  const IconBar({
    Key? key,
    required this.imageUrl,
    this.left = 0,
    this.right = 0,
    this.top = 0,
    this.bottom = 0,
    required this.square,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: square,
      height: square,
      margin:
          EdgeInsets.only(left: left, right: right, top: top, bottom: bottom),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            imageUrl,
          ),
        ),
      ),
    );
  }
}
