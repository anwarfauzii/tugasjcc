part of '../shared/theme.dart';

class ButtonSplashScreen extends StatelessWidget {
  final String route;
  final String title;
  final double topMargin;
  const ButtonSplashScreen(
      {Key? key,
      required this.title,
      required this.topMargin,
      this.route = '/'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 280,
      height: 35,
      margin: EdgeInsets.only(top: topMargin),
      decoration: BoxDecoration(
        boxShadow: [boxShadow],
        color: primaryColor,
        borderRadius: BorderRadius.circular(50),
      ),
      child: TextButton(
        onPressed: () => Navigator.pushNamed(context, route),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: const EdgeInsets.only(left: 28),
              child: Text(
                title,
                style: whiteTextStyle.copyWith(fontSize: 13, fontWeight: bold),
              ),
            ),
            Container(
              width: 47,
              height: 20,
              margin: const EdgeInsets.only(right: 24),
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/icon_arrow.png'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
