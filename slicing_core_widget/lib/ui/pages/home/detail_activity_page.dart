part of '../../../shared/theme.dart';

class DetailActivityPage extends StatelessWidget {
  const DetailActivityPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: 220,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/image_office_facebook.png'),
                ),
              ),
            ),
            SingleChildScrollView(
              child: Container(
                width: double.infinity,
                height: 700,
                padding: const EdgeInsets.symmetric(horizontal: 47),
                margin: const EdgeInsets.only(top: 200),
                decoration: BoxDecoration(
                  color: whiteColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Column(
                  children: [
                    const SizedBox(height: 32),
                    Text(
                      'Web Developer',
                      style: blackTextStyle.copyWith(
                          fontSize: 18, fontWeight: bold),
                    ),
                    const SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Facebook',
                          style: grey1TextStyle.copyWith(
                              fontSize: 13, fontWeight: bold),
                        ),
                        Container(
                          width: 3,
                          height: 3,
                          margin: const EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                              color: grey1Color, shape: BoxShape.circle),
                        ),
                        Text(
                          'Jakarta, INA',
                          style: grey1TextStyle.copyWith(
                              fontSize: 13, fontWeight: bold),
                        ),
                        Container(
                          width: 3,
                          height: 3,
                          margin: const EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                              color: grey1Color, shape: BoxShape.circle),
                        ),
                        Text(
                          '1 week ago',
                          style: grey1TextStyle.copyWith(
                              fontSize: 13, fontWeight: bold),
                        ),
                      ],
                    ),
                    const SizedBox(height: 23),
                    Container(
                      width: double.infinity,
                      height: 90,
                      padding: const EdgeInsets.all(14),
                      decoration: BoxDecoration(
                        color: whiteColor,
                        borderRadius: BorderRadius.circular(16),
                        border: Border.all(color: grey1Color),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.account_balance_wallet,
                                  color: primaryColor, size: 30),
                              const SizedBox(height: 10),
                              Text(
                                'IDR 8000K',
                                style: blackTextStyle.copyWith(
                                    fontSize: 13, fontWeight: bold),
                              ),
                            ],
                          ),
                          const VerticalDivider(thickness: 2),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.calendar_today_outlined,
                                  color: primaryColor, size: 30),
                              const SizedBox(height: 10),
                              Text(
                                'Fulltime',
                                style: blackTextStyle.copyWith(
                                    fontSize: 13, fontWeight: bold),
                              ),
                            ],
                          ),
                          const VerticalDivider(thickness: 2),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.supervisor_account_outlined,
                                  color: primaryColor, size: 30),
                              const SizedBox(height: 10),
                              Text(
                                'Senior',
                                style: blackTextStyle.copyWith(
                                    fontSize: 13, fontWeight: bold),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 14),
                    Container(
                      width: double.infinity,
                      height: 25,
                      decoration: BoxDecoration(
                        color: whiteColor,
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: grey1Color),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            width: 90,
                            height: 18,
                            decoration: BoxDecoration(
                              color: primaryColor,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Center(
                              child: Text(
                                'Requirement',
                                style: whiteTextStyle.copyWith(
                                    fontSize: 13, fontWeight: bold),
                              ),
                            ),
                          ),
                          const VerticalDivider(thickness: 2),
                          Container(
                            width: 90,
                            height: 18,
                            decoration: BoxDecoration(
                              color: whiteColor,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Text(
                              'About',
                              style: blackTextStyle.copyWith(
                                  fontSize: 13, fontWeight: bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 19),
                    SizedBox(
                      width: double.infinity,
                      child: Text(
                        'Job Description',
                        style: blackTextStyle.copyWith(
                            fontSize: 18, fontWeight: bold),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    const SizedBox(height: 5),
                    SizedBox(
                      width: double.infinity,
                      child: Text.rich(
                        TextSpan(
                            text:
                                'Facebook, Inc. adalah sebuah layanan jejaring sosial berkantor pusat di Menlo Park, California, Amerika Serikat yang diluncurkan pada bulan Februari 2004. Hingga September 2012, Facebook memiliki lebih dari satu miliar pengguna aktif,[11] lebih dari separuhnya menggunakan telepon genggam.[12] Pengguna harus mendaftar sebelum dapat....',
                            style: grey1TextStyle.copyWith(fontSize: 16),
                            children: [
                              TextSpan(
                                text: ' Show More',
                                style: primaryTextStyle.copyWith(
                                    fontSize: 16, fontWeight: bold),
                              ),
                            ]),
                      ),
                    ),
                    const SizedBox(height: 31),
                    const CustomTextButton(title: 'APPLY'),
                    const SizedBox(height: 60),
                  ],
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Opacity(
                  opacity: 0.5,
                  child: InkWell(
                    onTap: () => Navigator.pushNamed(context, '/home'),
                    child: Container(
                      width: 30,
                      height: 30,
                      margin: const EdgeInsets.only(top: 48, left: 36),
                      decoration: BoxDecoration(
                        color: blackColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Icon(
                          Icons.keyboard_arrow_left,
                          size: 14,
                          color: whiteColor,
                        ),
                      ),
                    ),
                  ),
                ),
                Opacity(
                  opacity: 0.5,
                  child: Container(
                    width: 30,
                    height: 30,
                    margin: const EdgeInsets.only(top: 48, right: 36),
                    decoration: BoxDecoration(
                      color: blackColor,
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.favorite_border,
                        size: 14,
                        color: whiteColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
