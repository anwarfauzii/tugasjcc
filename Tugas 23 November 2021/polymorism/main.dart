
import 'jenis_pemrograman.dart';
import 'pemrograman_java.dart';
import 'pemrograman_javascript.dart';
import 'pemrograman_php.dart';
import 'pemrograman_phyton.dart';

void main(){
  PemrogramanJava java = new PemrogramanJava();
  PemrogramanJavaScript javaScript = new PemrogramanJavaScript();
  PemrogramanPHP php = new PemrogramanPHP();
  PemrogramanPhyton phyton = new PemrogramanPhyton();

  JenisPemrograman jenispem = new PemrogramanPhyton();

  jenispem.tingkat_penguasaan();

  lamabelajar(phyton);
  lamabelajar(java);
  lamabelajar(javaScript);
  lamabelajar(php);

}

void lamabelajar (JenisPemrograman jp){
  jp.tingkat_penguasaan();
  jp.kegunaan();
}
