import 'jenis_pemrograman.dart';

class PemrogramanJava extends JenisPemrograman{
  @override
  void kegunaan(){
   print('Kegunaan Java untuk membangun Aplikasi');
  }
  void tingkat_penguasaan(){
    int penguasaanJava = 2;
    print('Tingkat Penguasaan saya di bahasa Pemrograman Java adalah $penguasaanJava');
  }
}    