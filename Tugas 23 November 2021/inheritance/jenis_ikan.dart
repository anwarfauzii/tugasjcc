class JenisIkan {
  int? _tenagaIkan;
  int get tenagaIkan => _tenagaIkan!;
  set tenagaIkan(int value) {
    if (value < 5) {
      value = 5;
    }
    _tenagaIkan = value;
  }
}
