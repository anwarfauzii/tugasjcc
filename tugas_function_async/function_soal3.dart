part of 'function.dart';

/// Function say merupakan function Opsional.
/// Function ini diatur dengan template ("Hello" + nama yang disapa + "from" + nama penyapa)
///
String say(String to, String from) {
  return "Hello " + to + " from " + from;
}
