part of 'function.dart';

/// Function line1 adalah function asynchronous
Future<String> line1() async {
  String lirik = "Seluruh kota merupakan tempat bermain yang asyik";
  return await Future.delayed(Duration(seconds: 1), () => lirik);
}

/// Function line2 adalah function asynchronous
Future<String> line2() async {
  String lirik = "Oh senangnya... ";
  return await Future.delayed(Duration(seconds: 1), () => lirik);
}

/// Function line3 adalah function asynchronous
Future<String> line3() async {
  String lirik = "aku senang sekali";
  return await Future.delayed(Duration(seconds: 1), () => lirik);
}

/// Function line4 adalah function asynchronous
Future<String> line4() async {
  String lirik = "Kalau begini akupun jadi sibuk";
  return await Future.delayed(Duration(seconds: 3), () => lirik);
}

/// Function line5 adalah function asynchronous
Future<String> line5() async {
  String lirik = "Berusaha mengejar-ngejar dia";
  return await Future.delayed(Duration(seconds: 1), () => lirik);
}

/// Function line6 adalah function asynchronous
Future<String> line6() async {
  String lirik = "Matahari menyinari semua perasaan cinta";
  return await Future.delayed(Duration(seconds: 1), () => lirik);
}

/// Function line7 adalah function asynchronous
Future<String> line7() async {
  String lirik = "Tapi mengapa hanya aku yang dimarahi";
  return await Future.delayed(Duration(seconds: 1), () => lirik);
}
