import 'package:feda_clover/shared/theme.dart';
import 'package:flutter/material.dart';

/// Class BoxOTP digunakan untuk mengisi box CodeOTP
class BoxOTP extends StatelessWidget {
  const BoxOTP({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10),
      child: Row(
        children: [
          Container(
            width: 62,
            height: 62,
            color: white2Color,
            child: TextFormField(
              style: blackTextStyle.copyWith(fontSize: 42, fontWeight: bold),
              textAlign: TextAlign.center,
              decoration: const InputDecoration.collapsed(
               hintText: '',
              ),
            ),
          )
        ],
      ),
    );
  }
}
