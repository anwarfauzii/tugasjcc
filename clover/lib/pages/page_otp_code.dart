import 'package:feda_clover/shared/bottom_navbar.dart';
import 'package:feda_clover/shared/theme.dart';
import 'package:feda_clover/widgets/box_otp.dart';
import 'package:flutter/material.dart';

class OTPCodePage extends StatelessWidget {
  /// Class OTPCodePage akan menampilkan layar pengisian Kode OTP yang akan dikirimkan ke user
  const OTPCodePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: const EdgeInsets.only(top: 116),
            child: Column(
              children: [
                Text(
                  'Konfirmasi Kode OTP',
                  style:
                      blackTextStyle.copyWith(fontSize: 32, fontWeight: bold),
                ),
                const SizedBox(height: 16),
                Text(
                  'Pesan berisi OTP telah dikirimkan ke email/no. telepon Anda. Kode OTP ini bersifat rahasia.',
                  style: grey3TextStyle.copyWith(fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      '5m',
                      style: grey1TextStyle.copyWith(fontSize: 16),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    // menampilkan box yang digunakan untuk mengisi kode OTP 
                    BoxOTP(),
                    BoxOTP(),
                    BoxOTP(),
                    BoxOTP(),
                  ],
                ),
                const SizedBox(height: 32),
                Container(
                  width: 109,
                  height: 42,
                  decoration: BoxDecoration(
                    color: green1Color,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'Konfirmasi',
                      style: white1TextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const BottomNavbar(),
        ],
      ),
    );
  }
}
